$(function(){
  var $registerForm = $("#registration")
  var $lofinForm = $("#loginForm")
  if($registerForm.length){
    $registerForm.validate({
      rules:{
        name:{
          required:true
        },
        username :{
          required : true,
          noSpace:true
        },
        password : {
          required:true
        },
        confirmpassword : {
          required : true,
          equalTo : '#psswd'
        }
      },
      messages:{
        name:{
          required:"Please enter your name"
        },
        username:{
          required:"Please Enter username"
        },
        password:{
          required:"Please Enter password"
        },
        confirmpassword:{
          required:"Please Re-Enter your password",
          equalTo:"Password didn't match"
        }
      }
    })
  }
})
